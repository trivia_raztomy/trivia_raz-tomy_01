#pragma once
#include <string>

#define ANSWER_NUM 4

using std::string;

class Question
{
public:
	Question(int id ,string question,string correctAnswer,string answer2,string answer3,string answer4); //set
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[ANSWER_NUM];
	int _correctAnswerIndex;
	int _id;

};
