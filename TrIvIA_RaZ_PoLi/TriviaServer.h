#pragma once

#define WIN32_LEAN_AND_MEAN

#include "Helper.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "DataBase.h"

#include <string>
#include <mutex>
#include <map>
#include <queue>
#include <vector>

#define SERVER_PORT 8820
#define ONE_BYTE 1
#define TWO_BYTES 2
#define FOUR_BYTES 4

using std::vector;
using std::string;
using std::mutex;
using std::map;
using std::queue;

class TriviaServer : public DataBase 
{
public:
	TriviaServer();//create SOCKET throw exception if crash
	~TriviaServer();// del from ddata rooms and connected users close socket
	void serve(); // run bindAndListen / create thread for clientHandler

private:
	void  bindAndListen();//do bind on socket and listen if crash somewhere throw EXCEPTION
	void accept();//create for every client thread
	void clientHandler(SOCKET client_socket);//use Helper::getMessageTypeCode for every msg not 0 create msg by buildRecieveMessage and add it to queue by addRecieveMessage
	void safeDeleteUser(RecievedMessage* msg);//disconnect user IN SAFE MODE \ get user socket and use handleSignout than close socket
	User* handleSignin(RecievedMessage* msg);//msg 200 by protocol / return user or nullptr if something worng\ msg have user and pass check by DateBase::isUserAndPassMatch if no snd to client msg / check if getUserByName and connected send error msg to client if not connected add to connected users and msg him for that
	void handleRecievedMessages();//use CV to know when queue null \take out msg from queue and than open mutex on queue\ use getUserBySocket \ if code for close connecion start safeDeleteUser or unknow or crash
	bool handleSignup(RecievedMessage* msg);//msg 203 / retrun true if succes / check if in msg user name /check if password correct by Validator::isPasswordValid and ok check username like that if everything ok check if this username already exist in DB DataBase::isUserExist if yes send error msg if no add to DB DateBase::addNewUser if crash send msg to client
	void handleSignout(RecievedMessage* msg);//msg 201 by protocol / check if msg have user / delete from connected users (handleLeaveRoom) and hnadleLeaveGame
	void handleLeaveGame(RecievedMessage* msg);//msg 222 / run leaveGame on user from him got msg if function return True mean game end and to remove from memory
	void handleStartGame(RecievedMessage* msg);//msg 217/ try create new game connected to user if crash send to user msg about it if succes create game del room from created game from list and use sendFirstQuestion
	void handlePlayerAnswer(RecievedMessage* msg);//msg 219 / get game by user if game != null run handleAnswerFromUser if false mean game end and need to clear
	bool handleCreateRoom(RecievedMessage* msg);//msg 213 / true if succes / if no user in msg DO NOTHING / rooms++ on user run createRoom if succes add to DB
	bool handleCloseRoom(RecievedMessage* msg);//msg 215 / do bool shit (boolshit :)) / get user room if no room return false run on user closeRoom 
	bool handleJoinRoom(RecievedMessage* msg);//msg 209/ bool shit(reutrn true if shit ok and false for bad shit)/ get user from msg if no so false / get room use getRoomById if found run joinRoom 
	bool handleLeaveRoom(RecievedMessage* msg);//msg 211/ boolshit/ user shit search / if ok run leaveRoom
	void handleGetUsersInRoom(RecievedMessage* msg);// 207 / get user shit / get ROOM ID use getRoomById / if ok run getUsersListMessage (*read book byurself too)
	void handleGetRooms(RecievedMessage* msg);//205 / run on all roms and create righ msg for rotocol and snd it to client
	void handleGetBestScores(RecievedMessage* msg);//223 / run DateBase::getBestScores and send it 
	void handleGetPersonalStatus(RecievedMessage* msg);//225 / run DateBase::getersonalStatus
	void addRecievedMessage(RecievedMessage* msg );//225 lock mutex using lock_guard and add msg to queue and unlock mutex and wwake up CV dat make thread work on msg
	RecievedMessage* buildRecievedMessage(SOCKET client_socket, int msgCode);//225 / create msg for the code and move to RecievedMessage
	User* getUserByName(string username); //search for user and return it
	User* getUserBySocket(SOCKET client_socket);//search for user AND RETURN THIS MOTHERFUCKER
	Room* getRoomById(int roomId);//search for room and return it

	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers; //map -> connected users socket/user
	map<string, string> _signedUsers;	//Map <username,password>
	DataBase _db;					//Find and include the library for this one
	map<int, Room*> _roomList; // map -> active rooms roomid/room
	mutex _mtxReceivedMessages;
	queue<RecievedMessage*> _queRcvMessages;//holds messages from clients 
	int _roomSequence; // Number of active rooms
};
