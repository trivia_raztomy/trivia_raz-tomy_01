#pragma once
#include <string>

#define MIN_PASS_LEN 4
#define SPACE_CHAR " "

using std::string;

class Validator
{
public:
	static bool isasswordValid(string password); //Validates password, At least 4 characters , no spaces, one digit, and one uppercase and lower case character.
	static bool isUsernameValid(string username);//Validates Username, Must start with a character, can't have spaces, can't be null 

};

