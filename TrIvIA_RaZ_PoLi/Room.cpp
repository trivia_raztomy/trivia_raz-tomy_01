#include "Room.h"
#include <string>
#include <iostream>
#include <Windows.h>
#include "User.h"
#include <string>
#include <vector>
#include <exception>

using std::string;
using std::vector;
using std::exception;	
using std::cout;

Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime) //set
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionsNo = questionNo;
	this->_questionTime = questionTime;
	this->_users.push_back(this->_admin);
	this->_numOfUsers = 1;
	this->_roomClosed = false;
}

Room::~Room()
{

}

bool Room::joinRoom(User* user)//return true if user join room false if no(FULL ROOM) / check if room full if yes snd to use msg about it if no add user and send msg for succes and than send to all users that he join (using sendMessage and getUsersListMessage)
{
	if (this->_roomClosed)
	{
		string failureMessage = "1102";

		try
		{
			user->send(failureMessage);		//Room's closed error message.
		}
		catch(...){}
		
		return false;
	}

	if (this->_numOfUsers < this->_maxUsers)	//Simple check if room's not full
	{
		this->_users.push_back(user);
		this->_numOfUsers++;
		
		string message = "1100";
		message += Helper::getPaddedNumber(this->_questionsNo, 2);
		message += Helper::getPaddedNumber(this->_questionTime, 2);

		try
		{
			user->send(message);
		}
		catch(...){}

		this->sendMessage(this->getUsersListMessage());
		return true;
	}
	else
	{
		string failureMessage = "1101";

		try
		{
			user->send(failureMessage);	//Send user a failure message (110 MESSAGE with 1 flag.
		}
		catch(...){}
		
		return false;
	}
}

void Room::leaveRoom(User* user)//search for user dat want to leave if found snd msg to him for leaving and to all users in room(without disconnect him)
{
	bool found = false;

	for (auto it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it)->getUsername() == user->getUsername())
		{
			found = true;
			this->_users.erase(it);
			break;
		}
	}

	if (found)
	{
		string leaveSuccessful = "1120";
		user->send(leaveSuccessful); //Send leaving successful message
		Room::sendMessage(Room::getUsersListMessage());	//User has already been erased from list, no need to use excluded
	}
	
}

int Room::closeRoom(User* user)// if succes to close return his ID if no -1 / check if user want to close room admin if no return -1 send all users msg about closing / run clearRoom on all users exclude admin
{
	string message = "116";	//Maybe this should look different

	if (user->getUsername() == this->_admin->getUsername())
	{
		for (auto it = this->_users.begin(); it != this->_users.end(); it++)
		{
			(*it)->send(message);
			if ((*it)->getUsername() != this->_admin->getUsername())
			{
				(*it)->clearRoom();
			}	
		}
		
		this->_roomClosed = true;
		return this->_id;
	}

	return -1;
}

vector<User*> Room::getUsers()
{
	return this->_users;
}

string Room::getUsersListMessage()//return msg all users in room (108)/ create msg
{
	string message = "108" + std::to_string(this->_numOfUsers);

	//If the room doesn't exist (Game has started, admin closed room), 
	//Message should looke like 1080 without any userNames in it.

	for (auto it = this->_users.begin(); it != this->_users.end(); it++)
	{
		message += Helper::getPaddedNumber((*it)->getUsername().length(), 2) + (*it)->getUsername();
	}

	return message;
}

int Room::getQUestionsNo()
{
	return this->_questionsNo;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}

int Room::getQuestionTime()
{
	return this->_questionTime;
}

//private:
string Room::getUsersAsString(vector<User*> userList, User* excludeUser)//ONLYFOR DEBUGING no need to use/ return all  users as string without exludeuser
{
	string users = "";

	for (auto it = userList.begin(); it != userList.end(); it++)
	{
		if ((*it)->getUsername() != excludeUser->getUsername())
		{
			users += (*it)->getUsername() + " ";
		}
	}

	return users;
}

void Room::sendMessage(string message)//Call send message, giving excludeUser as a null so the message will be sent to every user connected to this room.
{
	Room::sendMessage(nullptr, message);
}

void Room::sendMessage(User* excludeUser, string message)// Send message to all users except the excludedUser given as parameter
{
	try
	{
		if (excludeUser)
		{
			for (auto it = this->_users.begin(); it != this->_users.end(); it++)
			{
				if ((*it)->getUsername() != excludeUser->getUsername())
				{
					(*it)->send(message);
				}
			}
		}
		else
		{
			for (auto it = this->_users.begin(); it != this->_users.end(); it++)
			{
				(*it)->send(message);
			}
		}
		
	}
	catch (exception& e)
	{
		cout << __FUNCTION__ ": Exception " << e.what();
	}
}




        