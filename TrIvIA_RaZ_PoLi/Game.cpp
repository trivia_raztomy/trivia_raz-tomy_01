#include "Game.h"
#include <exception>
#include <sstream>

using std::map;
using std::vector;
using std::exception;

Game::Game(const vector<User*>& players, int questionNo, DataBase& db) : _db(db)  // try add game to DB using DataBase::insertNewGame if crash throw excetion / use DataBase::initQuestions / use User::setGame / *set everything*
{
	try
	{
		int res = db.insertNewGame();
		if (res != -1)
		{
			this->_questions_no = questionNo;
			this->_questions = db.initQuestions(questionNo);
			this->_Players = players;
			for (auto it = this->_Players.begin(); it != this->_Players.end(); it++)
			{
				this->_results[(*it)->getUsername()] = 0;
				(*it)->setGame(this);
			}
			
		}
		else
		{
			throw std::exception("error insert game");
		}
	}
	catch(...){}
}

Game::~Game()
{
	for (Question* i : this->_questions)
	{
		delete i;
	}
	if (this->_questions.size() != 0) this->_questions.erase(this->_questions.begin(), this->_questions.end());
	if (this->_Players.size() != 0) this->_Players.erase(this->_Players.begin(), this->_Players.end());
}

void Game::sendFirstQuestion()// start sendQUestionToAllUsers
{
	Game::sendQuestionToAllUsers();
}

bool Game::handleFinishGame()//update DB using DataBase::updateGameStatus and send all users (121) and set game to nullptr if crash catch it
{
	return false;
	try
	{
		bool updatestatus = _db.updateGameStatus(Game::getID());
		if (updatestatus)
		{
			try
			{
				for (auto it = this->_Players.begin(); it != this->_Players.end(); it++)
				{
					auto search = _results.find((*it)->getUsername()); // search for user in map
					string score = std::to_string(search->second);
					try
					{
						(*it)->send("121" + _Players.size() + Helper::getPaddedNumber(strlen((*it)->getUsername().c_str()), 2) + score); // create msg 121
					}
					catch(...){}
				}
			}
			catch (exception& e)
			{
				//Catch and keep on sending questions
			}
			//set game to nullptr
		}
	}
	catch (...)
	{

	}
}

bool Game::handleNextTurn()//retrun TRUE if game on/ check if users in game if no(use handleFinishGame)/check if all users answer on question this circle if yes check if last circle if this last end game if no go next question by 1 and snd to all users(sendQuestionToAllUsers) if still not all users ans DO NOTHING
{
	//IF GAME NOT END - >  RETRUN TRUE IF NO BLABLABLA SAME SHIT 
	//CHECK IF PLAYERS IN ROOM IF NO CALL HANDLEFINISHGAME 
	//IF PLAYERS IN ROOM CHECK IF ALL ANS IF YES CHECK IF LAST ROUND IF YES END IT IF NO GO NEXT + 1 AND SEND ALL  sendQuestionToAllUsers
	return true; //shit
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)//return TRUE if game not end false if end /set _currectTurnAnswers =+ 1/check if user chose right ans if yes _results += 1 / DataBase::addAnswerTolayer /if timeout add to DB null question / snd to user for right ans msg(120) turn on handleNextTurn
{
	Game* game = user->getGame();
	
	return false;
}

bool Game::leaveGame(User* currUser)//return TRUE if game on/False\ / search for user in DB if found him del and handleNextTurn
{
	return false;
}

int Game::getID()
{
	return 1;
}


//private:

bool Game::insertGameToDB()
{
	return false;
}

void Game::initQuestionsFromDB()
{

}

void Game::sendQuestionToAllUsers()//create msg 118 / set _currentTurnAnswers to 0 / send to all users if crash catch it
{
	this->_currentTurnAnswers = 0;
	Question* q = this->_questions.at(0);
	string QuestionString = q->getQuestion();

	try
	{
		for (auto it = this->_Players.begin(); it != this->_Players.end(); it++)
		{
			(*it)->send(QuestionString);
		}
	}
	catch (exception& e)
	{
		//Catch and keep on sending questions
	}
	/*
	code
	*/
}