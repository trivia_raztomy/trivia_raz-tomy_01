#include "DataBase.h"
#include <string>
#include "sqlite3.h"
#include <sys/timeb.h>
#include <ctime>
using std::endl;
using std::string;
using std::vector;
using std::to_string;
#define FILE_NAME R"(tri.db)"
DataBase::DataBase()
{

}

DataBase::~DataBase()
{

}
DataBase::DataBase(const DataBase& other)
{
	*this = other;
}
bool DataBase::isUserExist(string username)//check if exist in DB
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}
	char* errMsg = 0;
	int count = 0;
	string query = "select count(*) from t_users where username = \"" + username + "\"";
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackCount, &count, &errMsg) != SQLITE_OK)  // run the query, store output values in values vector
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}

	sqlite3_close(db);
	delete errMsg;
	return count;

}
bool  DataBase::addNewUser(string username, string password, string email) //save data to DB
{
	if (this->isUserExist(username)) return false;  // if the user exists return false (failed to add new User)

	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		return false;  // return false if failed
	}
	string query = "insert into t_users(username, password, email) values(";
	query += '"' + username + '"' + ", ";
	query += '"' + password + '"' + ", ";
	query += '"' + email + '"' + ')';

	char* errMsg = 0;

	if (sqlite3_exec(db, query.c_str(), DataBase::callbackCount, 0, &errMsg) != SQLITE_OK)  // run the query, store output values in values vector
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		delete errMsg;
		return false;  // return false if failed
	}
	return true;
}
bool DataBase::isUserAndPassMatch(string username, string password)//bool -> check if username correct with password
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}
	char* errMsg = 0;
	int count = 0;
	string query = "select count(*) from t_users where username = \"" + username + "\" and password = \"" + password + "\"";
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackCount, &count, &errMsg) != SQLITE_OK)  // run the query, store output values in values vector
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}
	sqlite3_close(db);
	delete errMsg;
	return count;
}
vector<Question*>  DataBase::initQuestions(int o) //random select question, o - > num question to 
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}
	char* errMsg = 0;
	vector<Question*> toRet;
	string query = "select * from t_questions";
	vector<Question*> vals;
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackQuestions, &vals, &errMsg) != SQLITE_OK)  // run the query, store output value in numOfQuestions
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}

	int i = 0;
	while (i < o && vals.size() != 0)
	{
		int gen = rand() % vals.size();
		toRet.push_back(vals[gen]);
		vals.erase(vals.begin() + gen);
		i++;
	}
	return toRet;
}

vector<string> DataBase::getBestScores()//like we want
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}

	char* errMsg = 0;
	vector<string> toRet;

	string query = "select username, sum(is_correct) from t_players_answers group by username order by sum(is_correct) desc limit 3;";
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackBestScores, &toRet, &errMsg) != SQLITE_OK)  // run the query, store output value in toRet
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}

	return toRet;
}

vector<string> DataBase::getPersonalStatus(string username) // like we want
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}

	char* errMsg = 0;
	vector<string> toRet;

	string query = "select count(distinct game_id) from t_players_answers group by username having username = \"" + username + "\"";
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackPersonalStatus, &toRet, &errMsg) != SQLITE_OK)  // run the query, store output value in toRet
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}

	query = "select sum(is_correct), count(is_correct), avg(answer_time) from t_players_answers where username = \"" + username + "\"";
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackPersonalStatus, &toRet, &errMsg) != SQLITE_OK)  // run the query, store output value in toRet
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}
	return toRet;
}

int  DataBase::insertNewGame()//return num room, create new game table, status game = 0H, start = NOW 
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}

	char* errMsg = 0;
	string query = "insert into t_games(status, start_time) values(0, '";
	time_t rawTime = std::time(0);
	tm* currTime = new tm;
	localtime_s(currTime, &rawTime);

	string year = std::to_string(1900 + currTime->tm_year);  
	string month = Helper::getPaddedNumber(currTime->tm_mon + 1, 2);  
	string day = Helper::getPaddedNumber(currTime->tm_mday, 2);
	string hour = Helper::getPaddedNumber(currTime->tm_hour, 2);
	string min = Helper::getPaddedNumber(currTime->tm_min, 2);
	string sec = Helper::getPaddedNumber(currTime->tm_sec, 2);

	delete currTime;
	string timeStr = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':' + sec;
	query += timeStr + "');\n";
	query += "select game_id from t_games where start_time = '" + timeStr + '\'';

	int id = 0;
	if (sqlite3_exec(db, query.c_str(), DataBase::callbackCount, &id, &errMsg) != SQLITE_OK)  // run the query, no need to pass callback function because there is no output
	{
		sqlite3_close(db);
		std::cout << "SQL ERROR: " << errMsg << endl;
		throw std::exception(errMsg);
	}
	sqlite3_close(db);
	delete errMsg;
	return id;
}

bool DataBase::updateGameStatus(int gameId)//return if succes or no, update status to 1 and end time to NOW
{
	sqlite3* db;
	char* errMsg;
	sqlite3_stmt *statement;
	if (sqlite3_open("", &db)) // <<add file name 
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}
	string query = "UPDATE t_games SET status = 1, end_time = NOW WHERE game_id = ";
	query += std::to_string(gameId);
	const char* com = query.c_str();
	if (sqlite3_exec(db, query.c_str(), nullptr, nullptr, &errMsg) != SQLITE_OK) //<<error fuck it 
	{
		sqlite3_close(db);
		return false;
	}
	sqlite3_close(db);
	return true;//shit
}

bool  DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)//return if succes or no , add
{
	sqlite3* db;
	if (sqlite3_open(FILE_NAME, &db))  // TRY to open db
	{
		sqlite3_close(db);
		throw std::exception(sqlite3_errmsg(db));
	}
	string query = "insert into t_players_answers (game_id, username, question_id, player_answer, is_correct, answer_time) values(";
	query += to_string(gameId) + ", " +
		"\"" + username + "\", " +
		to_string(questionId) + ", " +
		"\"" + answer + "\", " +
		(isCorrect ? "1" : "0") + ", " +
		to_string(answerTime) + ")";
	char* errMsg;
	if (sqlite3_exec(db, query.c_str(), nullptr, nullptr, &errMsg) != SQLITE_OK)
	{
		sqlite3_close(db);
		throw std::exception(errMsg);
	}
	sqlite3_close(db);
	return false;
}

//private:
int DataBase::callbackCount(void* output, int argc, char** argv, char** azCol)
{
	*(int*)output = std::stoi(argv[0]);
	return 0;
}
int DataBase::callbackQuestions(void* output, int argc, char** argv, char** azCol)
{
	((vector<Question*>*)output)->push_back(new Question(atoi(argv[0]),argv[1],argv[2],argv[3],argv[4],argv[5]));
	return 0;
}

int DataBase::callbackBestScores(void* output, int argc, char** argv, char** azCol)
{
	string name = argv[0];
	string score = argv[1];
	((vector<string>*)output)->push_back(Helper::getPaddedNumber(name.length(), 2) + name + Helper::getPaddedNumber(std::stoi(score), 6));
	return 0;
}

int DataBase::callbackPersonalStatus(void* output, int argc, char** argv, char** azCol)
{
	((vector<string>*) output)->push_back(argv[0]);

	//if the output is equal to 3 collumns, push back the substruction of the second col from the first col: basically the num of games minus the num of right answeres
	if (argc == 3)
	{
		string Time_string = argv[2];
		int dotIndex = Time_string.find(".");
		string wholePart = Time_string.substr(0, dotIndex), fractionPart = Time_string.substr(dotIndex + 1, 3 - dotIndex);
		((vector<string>*) output)->push_back(to_string(std::stoi(argv[1]) - std::stoi(argv[0])));
		((vector<string>*) output)->push_back(Helper::getPaddedNumber(std::stoi(wholePart), 2) + Helper::getPaddedNumber(std::stoi(fractionPart), 2));
	}
	return 0;
}