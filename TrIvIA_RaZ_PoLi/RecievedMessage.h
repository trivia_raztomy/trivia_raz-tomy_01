#pragma once

#define WIN32_LEAN_AND_MEAN

#include "User.h"
#include <string>
#include <vector>


using std::string;
using std::vector;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET socket,int messageCode);
	RecievedMessage(SOCKET socket,int messageCode,vector<string> values);
	SOCKET getSocket();
	User* getUser(); 
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();

private:
	SOCKET _socket;
	User* _user;
	int _messageCode;
	vector<string> _values;
};

