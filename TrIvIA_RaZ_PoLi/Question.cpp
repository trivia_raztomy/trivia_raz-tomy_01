#include "Question.h"
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>

using std::string;

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	std::srand(std::time(nullptr));
	this->_correctAnswerIndex = (std::rand() % ANSWER_NUM) - 1;			// (1,4), get it as an index number.
	this->_id = id;
	this->_question = question;

	this->_answers[this->_correctAnswerIndex] = correctAnswer;
	switch (this->_correctAnswerIndex)
	{
		case 1:								//Correct is in 0 index
			this->_answers[1] = answer2;
			this->_answers[2] = answer3;
			this->_answers[3] = answer4;
			break;
		case 2:								
			this->_answers[0] = answer2;
			this->_answers[2] = answer3;
			this->_answers[3] = answer4;
			break;
		case 3:
			this->_answers[0] = answer2;
			this->_answers[1] = answer3;
			this->_answers[3] = answer4;
			break;
		case 4:
			this->_answers[0] = answer2;
			this->_answers[1] = answer3;
			this->_answers[2] = answer4;
			break;
	}
}

string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctAnswerIndex;
}

int Question::getId()
{
	return this->_id;
}
