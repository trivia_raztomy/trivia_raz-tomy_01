#pragma comment (lib, "ws2_32.lib")

#include "TriviaServer.h" 
#include <string>
#include <iostream>
#include <exception>
#include <thread>
#include <mutex>
#include <condition_variable>

using std::string;
using std::thread;
using std::exception;
using std::cout;
using std::endl;
using std::unique_lock;
using std::condition_variable;
using std::mutex;
using std::lock_guard;

mutex messagesLock;
condition_variable messagesCond;

TriviaServer::TriviaServer()
{
	this->_roomSequence = 0;
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
	
}

TriviaServer::~TriviaServer()
{
	try
	{
		//Delete from data rooms and connected users or something didn't understand what you wrote.
		//Maybe kill all running threads, or somehow wait for MessageHandler thread to finish current work and then kill that mofo
		::closesocket(this->_socket);
	}
	catch (...)
	{}
}

void TriviaServer::serve()
{
	try
	{
		this->bindAndListen();
	}
	catch (exception& e){}

	//Create new thread for messageHandling
	thread messagesHandler(&TriviaServer::handleRecievedMessages, this);
	messagesHandler.detach();

	while (true)
	{
		this->accept();
	}
}

void  TriviaServer::bindAndListen()//do bind on socket and listen if crash somewhere throw EXCEPTION
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(SERVER_PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	//Configuring socket 
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw exception(__FUNCTION__ " - listen");

	cout << "Binded and listening on port: " << SERVER_PORT << endl;
}

void TriviaServer::accept()//create for every client thread
{
	//Accepts and create socket between client and server
	SOCKET client_socket = ::accept(_socket, NULL, NULL);			

	if (client_socket == INVALID_SOCKET)
		throw exception(__FUNCTION__ " - InvalidClientSocket");

	cout << "Client accepted " << endl;

	//Create new thread for the connected client
	try
	{
		thread clientHandler(&TriviaServer::clientHandler, this, client_socket);	
		clientHandler.detach();

		cout << "Client handler created " << endl;
	}
	catch (exception& e)
	{
		cout << "Error in TriviaServer::accept(): " << e.what();
	}
	
}

void TriviaServer::clientHandler(SOCKET client_socket)//use Helper::getMessageTypeCode for every msg not 0 create msg by buildRecieveMessage and add it to queue by addRecieveMessage
{
	try
	{
		int messageCode = Helper::getMessageTypeCode(client_socket);
		cout << "Client handler started " << endl;

		while (messageCode != 0 || messageCode != 299)	//User disconnected or Exited Application
		{
			RecievedMessage* msg = nullptr;
			vector<string> values;

			switch (messageCode)
			{
			case 200:	//Signin request
				try
				{
					string username = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));
					string password = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));

					values.push_back(username);
					values.push_back(password);
				}
				catch (exception& e)
				{}

				break;

			case 203:	//Signup request
				try
				{
					string username = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));
					string password = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));
					string email = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));

					values.push_back(username);
					values.push_back(password);
					values.push_back(email);
				}
				catch (exception& e)
				{}

				break;

			case 207:
				try
				{
					string roomId = Helper::getStringPartFromSocket(client_socket, FOUR_BYTES);	//roomId length is 4 bytes, 
					values.push_back(roomId);
				}
				catch (exception& e)
				{}

				break;

			case 209:
				try
				{
					string roomId = Helper::getStringPartFromSocket(client_socket, FOUR_BYTES);	//roomId length is 4 bytes, 
					values.push_back(roomId);
				}
				catch (exception& e)
				{}

				break;

			case 213:
				try
				{
					string roomName = Helper::getStringPartFromSocket(client_socket, std::stoi(Helper::getStringPartFromSocket(client_socket, TWO_BYTES)));
					string numOfPlayersInRoom = Helper::getStringPartFromSocket(client_socket, ONE_BYTE);
					string questionsNum = Helper::getStringPartFromSocket(client_socket, TWO_BYTES);
					string questionsAnswerTime = Helper::getStringPartFromSocket(client_socket, TWO_BYTES);

					values.push_back(roomName);
					values.push_back(numOfPlayersInRoom);
					values.push_back(questionsNum);
					values.push_back(questionsAnswerTime);
				}
				catch (exception& e)
				{}

				break;

			case 219:
				try
				{
					string ansNum = Helper::getStringPartFromSocket(client_socket, ONE_BYTE);		//If the player didn't answer in time, the ansNum will be 5
					string questionsAnswerTime = Helper::getStringPartFromSocket(client_socket, TWO_BYTES);

					values.push_back(ansNum);
					values.push_back(questionsAnswerTime);
				}
				catch (exception& e)
				{}

				break;
			}

			if (!values.empty())
			{
				msg = new RecievedMessage(client_socket, messageCode, values);
			}
			else
			{
				msg = new RecievedMessage(client_socket, messageCode);
			}

			TriviaServer::addRecievedMessage(msg);

			cout << "Message pushed to queue by clientHandler " << endl;


			messageCode = Helper::getMessageTypeCode(client_socket);
		}

		cout << "End of conversation with client" << endl;

		//Create end of conversation message and push to messagesQueue.
		RecievedMessage* endMsg = new RecievedMessage(client_socket, 299);
		TriviaServer::addRecievedMessage(endMsg);
	}
	catch (exception& e)
	{
		cout << "ERROR: " __FUNCTION__ << " " << e.what() << endl;
		RecievedMessage* endMsg = new RecievedMessage(client_socket, 299);
		TriviaServer::addRecievedMessage(endMsg);
	}

	
}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)//disconnect user IN SAFE MODE \ get user socket and use handleSignout than close socket
{
	try
	{
		SOCKET userSocket = msg->getSocket();
		TriviaServer::handleSignout(msg);
		::closesocket(userSocket);

		if (msg->getUser() != nullptr)
		{
			this->_connectedUsers.erase(this->_connectedUsers.find(userSocket));
		}
	}
	catch (...)
	{
		//Handle exception here, don't throw it.
	}
}

User* TriviaServer::handleSignin(RecievedMessage* msg)//msg 200 by protocol / return user or nullptr if something worng\ msg have user and pass check by DateBase::isUserAndPassMatch if no snd to client msg / check if getUserByName and connected send error msg to client if not connected add to connected users and msg him for that
{
	//Function works properly.
	try
	{
		vector<string> temp = msg->getValues();
		User* curr = msg->getUser();

		if (curr != nullptr)
		{
			//User exists, and is already connected
			curr->send("1022");		//Send error message
			return nullptr;
		}
		else
		{
			string username = temp.at(0);
			string password = temp.at(1);
			if (this->_signedUsers[username] == password)
			{
				this->_connectedUsers[msg->getSocket()] = new User(username, msg->getSocket());
				this->_connectedUsers[msg->getSocket()]->send("1020");
				return this->_connectedUsers[msg->getSocket()];
			}
			else
			{
				Helper::sendData(msg->getSocket(), "1021");
				return nullptr;
			}	
		}
	}
	catch (...)
	{
		return nullptr;
	}
}
void TriviaServer::handleRecievedMessages()//use ConditionVariable to know when queue null \take out msg from queue and than open mutex on queue\ use getUserBySocket \ if code for close connecion start safeDeleteUser or unknow or crash
{
	while (true)	//Need to change this condition. When should it stop?
	{
		try
		{
			unique_lock<mutex> messagesLocker(messagesLock);	//First use of mutex in code
			messagesCond.wait(messagesLocker);
			RecievedMessage* ms = this->_queRcvMessages.front();
			this->_queRcvMessages.pop();
			messagesLocker.unlock();
			
			if (TriviaServer::getUserBySocket(ms->getSocket()) != nullptr)
			{
				ms->setUser(TriviaServer::getUserBySocket(ms->getSocket()));
			}
			else
			{
				ms->setUser(nullptr);
			}

			thread* t = nullptr;

			switch (ms->getMessageCode())
			{
				case 200:	//Sign in request
					t = new thread(&TriviaServer::handleSignin, this, std::ref(ms));	//Returs User*
					t->detach();
					break;
				case 201:	//Sign out request
					t = new thread(&TriviaServer::handleSignout, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 203:
					t = new thread(&TriviaServer::handleSignup, this ,std::ref(ms));	//Bool
					t->detach();
					break;
				case 205:
					t = new thread(&TriviaServer::handleGetRooms, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 207:
					t = new thread(&TriviaServer::handleGetUsersInRoom, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 209:
					t = new thread(&TriviaServer::handleJoinRoom, this, std::ref(ms));	//Bool
					t->detach();
					break;
				case 211:
					t = new thread(&TriviaServer::handleLeaveRoom, this, std::ref(ms));	//Bool
					t->detach();
					break;
				case 213:
					t = new thread(&TriviaServer::handleCreateRoom, this, std::ref(ms)); //Bool
					t->detach();
					break;
				case 215:
					t = new thread(&TriviaServer::handleCloseRoom, this, std::ref(ms)); //Bool
					t->detach();
					break;
				case 217:
					t = new thread(&TriviaServer::handleStartGame, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 219:
					t = new thread(&TriviaServer::handlePlayerAnswer, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 222:
					t = new thread(&TriviaServer::handleLeaveGame, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 223:
					t = new thread(&TriviaServer::handleGetBestScores, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 255:
					t = new thread(&TriviaServer::handleGetPersonalStatus, this, std::ref(ms));	//Void
					t->detach();
					break;
				case 299:
					// Client have finished communication
					//SafeDeleteUser
					TriviaServer::safeDeleteUser(ms);
					break;	
				default:
					TriviaServer::safeDeleteUser(ms);
					break;
			}
		}
		catch (exception& e)
		{
			cout << "ERROR IN " << __FUNCTION__ ": " << e.what() << endl;	
			//SafeDeleteUser
		}
	}
}

bool TriviaServer::handleSignup(RecievedMessage* msg)//msg 203 / retrun true if succes / check if in msg user name /check if password correct by Validator::isPasswordValid and ok check username like that if everything ok check if this username already exist in DB DataBase::isUserExist if yes send error msg if no add to DB DateBase::addNewUser if crash send msg to client
{
	//Function works properly.
	string message = "104";

	try
	{
		vector<string> temp = msg->getValues();
		//temp.at(1)
		if (Validator::isUsernameValid(temp.at(0)))
		{
			if (Validator::isasswordValid(temp.at(1)))
			{
				/*FOR FUTURE SEND MSG TO EMAIL*/
				if (this->_signedUsers.find(temp.at(0)) != this->_signedUsers.end())  //DataBase::isUserExist(temp.at(0))
				{
					//msg user already exist
					message += "2";
					Helper::sendData(msg->getSocket(), message);
					return false;
				}
				else
				{
					//DataBase::assNewUser(temp.at(0), temp.at(1), temp.at(2));
					this->_signedUsers[temp.at(0)] = temp.at(1);
					message += "0";
					//Send message to user using the socket from the recieved message
					Helper::sendData(msg->getSocket(), message);
					return true;
				}
			}
			else
			{
				//message ilegal password
				message += "1";
				Helper::sendData(msg->getSocket(), message);
				return false;
			}
		}
		else
		{
			//message Wrong Username
			message += "3";
			Helper::sendData(msg->getSocket(), message);
			return false;
		}
	}
	catch (...)
	{
		message += "4";
		Helper::sendData(msg->getSocket(), message);
		return false;
	}
}

void TriviaServer::handleSignout(RecievedMessage* msg)//msg 201 by protocol / check if msg have user / delete from connected users (handleLeaveRoom) and hnadleLeaveGame
{
	//Run tests
	if (msg->getUser() != nullptr)
	{
		this->_connectedUsers.erase(msg->getUser()->getSocket());
		TriviaServer::handleCloseRoom(msg);
		TriviaServer::handleLeaveRoom(msg);
		TriviaServer::handleLeaveGame(msg);
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)//msg 222 / run leaveGame on user from him got msg if function return True mean game end and to remove from memory
{
	User* user = msg->getUser();

	if (user != nullptr)
	{
	
		Game* game = user->getGame();

		if (game != nullptr)
		{
			bool res = user->leaveGame();
			if (res)
			{
				user->getGame()->~Game();
			}
		}
	}
	
}

void TriviaServer::handleStartGame(RecievedMessage* msg)//msg 217/ try create new game connected to user if crash send to user msg about it if succes create game del room from created game from list and use sendFirstQuestion
{
	User* user = TriviaServer::getUserBySocket(msg->getSocket()); //admin user
	string errorMsg = "";	//Find correct details

	try
	{



	}
	catch(...)
	{
		if (user != nullptr)
		{
			user->send(errorMsg);
		}
		else
		{
			Helper::sendData(msg->getSocket(), errorMsg);
		}
		// send error msg to user
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)//msg 219 / get game by user if game != null run handleAnswerFromUser if false mean game end and need to clear
{

}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)//msg 213 / true if succes / if no user in msg DO NOTHING / rooms++ on user run createRoom if succes add to DB
{
	vector<string>  temp = msg->getValues();
	User* currUser = TriviaServer::getUserBySocket(msg->getSocket());

	if (currUser != nullptr)
	{
		try
		{
			int roomId = std::stoi(Helper::getPaddedNumber(this->_roomSequence, FOUR_BYTES));
			bool res = currUser->createRoom(roomId, temp.at(0), std::stoi(temp.at(1)), std::stoi(temp.at(2)), std::stoi(temp.at(3)));

			if (res)
			{
				// add to DB  
				this->_roomList[roomId] = currUser->getRoom();
				this->_roomSequence++;
			}
			
			return res;
		}
		catch (...) {}
	}
	else
	{
		return false;
	}
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)//msg 215 / do bool shit (boolshit :)) / get user room if no room return false run on user closeRoom 
{
	User* user = msg->getUser();

	if (user == nullptr || user->getRoom() == nullptr)
	{
		return false;
	}

	int res = user->closeRoom();

	if (res != ERROR)
	{
		this->_roomList.erase(res);
		return true;
	}
}

bool TriviaServer::handleJoinRoom(RecievedMessage* msg)//msg 209/ bool shit(reutrn true if shit ok and false for bad shit)/ get user from msg if no so false / get room use getRoomById if found run joinRoom 
{
	User* user = msg->getUser();
	if (user == nullptr)
	{
		return false;
	}

	string roomIdStr = msg->getValues().at(0);
	int roomId = std::stoi(roomIdStr);
	Room* room = TriviaServer::getRoomById(roomId);

	if (room != nullptr)
	{
		bool success = false;

		try
		{
			success = user->joinRoom(room);
		}
		catch(...){}

		return success;
	}

	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)//msg 211/ boolshit/ user shit search / if ok run leaveRoom
{
	User* user = msg->getUser();
	string failMessage = "116";
	if (user == nullptr)
	{
		//Send failure message;
		try
		{
			Helper::sendData(msg->getSocket(), failMessage);
		}
		catch(...){}

		return false;
	}
	Room* room = user->getRoom();
	if (room != nullptr)
	{
		user->leaveRoom();

		return true;
	}

	//Send failure message;
	user->send(failMessage);
	return false;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)// 207 / get user shit / get ROOM ID use getRoomById / if ok run getUsersListMessage (*read book byurself too)
{
	User* user = msg->getUser();
	string errorMsg = "1080";

	try
	{
		int roomIdInt = std::stoi(msg->getValues().at(0));
		Room* room = getRoomById(roomIdInt);
		
		if (room != nullptr)
		{
			if (user != nullptr)
			{
				user->send(room->getUsersListMessage());
			}
			else
			{
				Helper::sendData(msg->getSocket(), room->getUsersListMessage());
			}
		}
		else if(user != nullptr)
		{
			user->send(errorMsg);
		}
		else
		{
			Helper::sendData(msg->getSocket(), errorMsg);
		}
	}
	catch (...) 
	{
		if (user != nullptr)
		{
			user->send(errorMsg);
		}
		else
		{
			try
			{
				Helper::sendData(msg->getSocket(), errorMsg);
			}
			catch(...){}
		}
	}
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)//205 / run on all roms and create righ msg for rotocol and snd it to client
{
	User* user = msg->getUser();
	string newmsg = "106" + Helper::getPaddedNumber(this->_roomSequence,FOUR_BYTES);
	for (auto it = _roomList.begin(); it != _roomList.end(); it++)
	{
		string roombyte = Helper::getPaddedNumber((*it).second->getName().length(), TWO_BYTES);
		string roomId = Helper::getPaddedNumber((*it).first, FOUR_BYTES);
		newmsg +=  roomId + roombyte + (*it).second->getName();	//std::to_string((*it).first)
	}
	if (user != nullptr)
	{
		user->send(newmsg);
	}
	else
	{
		Helper::sendData(msg->getSocket(), newmsg);
	}
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)//223 / run DateBase::getBestScores and send it 
{
	User* user = msg->getUser();
	vector<string> best = DataBase::getBestScores();
	string nmsg = "124";
	//...//
	user->send(nmsg);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* msg)//225 / run DateBase::getersonalStatus
{

}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)//225 lock mutex using lock_guard and add msg to queue and unlock mutex and wwake up CV dat make thread work on msg
{
	try
	{
		unique_lock<mutex> messagesLockGuard(messagesLock);	
		this->_queRcvMessages.push(msg);	//Crital line
		messagesLockGuard.unlock();

		messagesCond.notify_one();	//Notifies handeRecivedMessage's CV, when function ends, the lockGuard will be unlocked.
	}
	catch (exception& e)
	{
		cout << "ERROR " << __FUNCTION__ << ": " << e.what() << endl;
	}
}

RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET client_socket, int msgCode)//225 / create msg for the code and move to RecievedMessage
{
	RecievedMessage* msg = new RecievedMessage(client_socket, msgCode);
	msg->setUser(TriviaServer::getUserBySocket(client_socket));

	return msg;
}

User* TriviaServer::getUserByName(string username) //search for user and return it
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if ((*it).second->getUsername() == username)
		{
			return (*it).second;
		}
	}

	return nullptr;
}

User* TriviaServer::getUserBySocket(SOCKET client_socket)//search for user AND RETURN THIS MOTHERFUCKER
{
	for (auto it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); it++)
	{
		if ((*it).first == client_socket)
		{
			return (*it).second;
		}
	}

	return nullptr;
}

Room* TriviaServer::getRoomById(int roomId)//search for room and return it
{
	for (auto it = this->_roomList.begin(); it != this->_roomList.end(); it++)
	{
		if ((*it).first == roomId)
		{
			return (*it).second;
		}
	}

	return nullptr;
}

//std::thread t((&viewWindow::refreshWindow, render, playerRect, backTexture, playerTexture)); BAD

//std::thread t(&viewWindow::refreshWindow, window, render, std::ref(playerRect), backTexture, playerTexture); GOOD

//viewWindow::refreshWindow(renderer, playerRect, backTexture, playerTexture);	PARAMS
