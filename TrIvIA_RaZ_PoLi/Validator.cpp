#include "Validator.h"
#include <string>
#include <cctype>

using std::string;

bool Validator::isasswordValid(string password)
{
	if (password.length() < MIN_PASS_LEN)
	{
		return false;
	}
	bool hasUpperCase = false, hasLowerCase = false;

	for (auto it = password.begin(); it != password.end(); it++)
	{
		if (isspace((*it)))
		{
			return false;
		}
		else if (isupper((*it)))
		{
			hasUpperCase = true;
		}
		else if (islower((*it)))
		{
			hasLowerCase = true;
		}

	}

	if (password.find_first_of("0123456789") != std::string::npos && hasLowerCase && hasUpperCase)
	{
		return true;
	}

	return false;
}

bool Validator::isUsernameValid(string userName)
{
	if (userName == "")
	{
		return false;
	}

	for (auto it = userName.begin(); it != userName.end(); it++)
	{
		if (isspace((*it)))
		{
			return false;
		}
	}

	return true;
}