#include <iostream>
#include "TriviaServer.h"
#include "WSAInitializer.h"
#include <iostream>

using std::cout;
using std::endl;
using std::exception;

int main()
{
	try
	{
		WSAInitializer* wsa = new WSAInitializer();

		TriviaServer* server = new TriviaServer();
		server->serve();

		delete wsa;

		std::terminate();
	}
	catch (exception& e)
	{
		cout << "Exception occured in main: " << e.what() << endl;
	}
	

	return 1;
}