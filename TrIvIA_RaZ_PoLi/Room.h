#pragma once

#define WIN32_LEAN_AND_MEAN

#include "User.h"
#include <string>
#include <vector>
#include <sstream>

using std::string;
using std::vector;

class User;
class Room 
{
public:
	~Room();
	Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime); //set
	bool joinRoom(User* user);//return true if user join room false if no(FULL ROOM) / check if room full if yes snd to use msg about it if no add user and send msg for succes and than send to all users that he join (using sendMessage and getUsersListMessage)
	void leaveRoom(User* user);//search for user dat want to leave if found snd msg to him for leaving and to all users in room(without disconnect him)
	int closeRoom(User* user);// if succes to close return his ID if no -1 / check if user want to close room admin if no return -1 send all users msg about closing / run clearRoom on all users exclude admin
	vector<User*> getUsers(); //like we want
	string getUsersListMessage();//return msg all users in room (108)/ create msg
	int getQUestionsNo();// like we want
	int getId();// like we want
	string getName(); // like we want
	int getQuestionTime();
private:
	string getUsersAsString(vector<User*> userList, User* excludeUser);//ONLYFOR DEBUGING no need to use/ return all  users as string without exludeuser
	void sendMessage(string message);//snd msg
	void sendMessage(User* excludeUser, string message);// sennd to all users msg excllude user

	vector<User*> _users; //users connected to room
	User* _admin;//point to admin user
	int _maxUsers; //max users in room
	int _questionTime;//time for question
	int _questionsNo; //mmax question
	std::string _name; //room name
	int _id;//room id
	int _numOfUsers; 
	bool _roomClosed;
};
