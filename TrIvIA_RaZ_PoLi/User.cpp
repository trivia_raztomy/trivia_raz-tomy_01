#pragma comment (lib, "ws2_32.lib")

#include "User.h"
#include "Game.h"
#include "Helper.h"
#include <exception>
#include <iostream>

using std::string;
using std::exception;
using std::cout;

User::User(string username, SOCKET sock) // set, room and game set to nullptr
{
	this->_username = username;
	this->_sock = sock;
	this->_currGame = nullptr;
	this->_currRoom = nullptr;
}

void User::send(string message)//send msg to user / use Heler::sendData
{
	try
	{
		Helper::sendData(this->_sock, message);
	}
	catch (exception& e)
	{
		cout << "Error: " __FUNCTION__ << " " << e.what();
	}
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* gm)
{
	this->_currGame = gm;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionTim, int questionNo) // return if succes, check if user connected to room if yes send msg if crash protocol - code 114
{
	string message = "114";

	if (this->_currRoom != nullptr)
	{
		message += "1";
		this->send(message);	//1141 roomCreationFailed

		return false;
	}

	this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionNo, questionTim);
	message += "0";	//1140 roomCreationIsSuccessful
	this->send(message);

	return true;
}

bool User::joinRoom(Room* newRoom)
{
	if (this->_currRoom != nullptr)
	{
		return false;
	}
	
	bool joinedRoom = newRoom->joinRoom(this);
	
	if (joinedRoom)
	{
		this->_currRoom = newRoom;
		return true;
	}

	return joinedRoom;
}

void User::leaveRoom()// if user connected to room and set to nullptr if yes
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
	}
}

int User::closeRoom()//return num room close and -1 if crash / if user not in room -1 / set to nullptr room
{
	if (this->_currRoom == nullptr)
	{
		return WRONG;
	}

	int res = this->_currRoom->closeRoom(this);

	if (res != WRONG)
	{									/*****************************************************************************************/
		this->_currRoom->~Room();	// /* LOOK HERE */ Delete room from memory. Maybe there's another way to do it /* LOOK HERE */
		this->_currRoom = nullptr;	  /*****************************************************************************************/
		
		return res;
	}

	return res;
}

bool User::leaveGame()//return TRUE if game online \false if end // set to nullptr
{
	if (this->_currRoom != nullptr)
	{
		bool res = this->_currGame->leaveGame(this);
		if (res)
		{
			this->_currGame = nullptr;
			return res;
		}
		return res;
	}

	return false;
}