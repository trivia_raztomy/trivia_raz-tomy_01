#pragma once

#include "DataBase.h"
#include "Question.h"
#include "User.h"
#include <map>
#include <vector>
#include <algorithm> 
#include <functional>

using std::map;
using std::vector;
using std::mem_fun_ref;
using std::bind2nd;
using std::for_each;

class User;
class Game 
{
public:
	Game(const vector<User*>& players, int questionNo, DataBase& db);// try add game to DB using DataBase::insertNewGame if crash throw excetion / use DataBase::initQuestions / use User::setGame / *set everything*
	~Game();//clear data
	void sendFirstQuestion();// start sendQUestionToAllUsers
	bool handleFinishGame();//update DB using DataBase::updateGameStatus and send all users (121) and set game to nullptr if crash catch it
	bool handleNextTurn();//retrun TRUE if game on/ check if users in game if no(use handleFinishGame)/check if all users answer on question this circle if yes check if last circle if this last end game if no go next question by 1 and snd to all users(sendQuestionToAllUsers) if still not all users ans DO NOTHING
	bool handleAnswerFromUser(User* user, int answerNo, int time);//return TRUE if game not end false if end /set _currectTurnAnswers =+ 1/check if user chose right ans if yes _results += 1 / DataBase::addAnswerTolayer /if timeout add to DB null question / snd to user for right ans msg(120) turn on handleNextTurn
	bool leaveGame(User* currUser);//return TRUE if game on/False\ / search for user in DB if found him del and handleNextTurn
	int getID();
	
private:

	vector<Question*> _questions;
	vector<User*> _Players;
	int _questions_no;
	int _currQuestionsIndex;
	DataBase& _db;
	map<string, int> _results;// map - > user and his score
	int _currentTurnAnswers;// num users return ans on question
	//-----------------------
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();//create msg 118 / set _currentTurnAnswers to 0 / send to all users if crash catch it

};
