#pragma once

#include <WinSock2.h>
#include <string>
#include "Game.h"
#include "Room.h"
#include "Helper.h"
#include <iostream>
#define WRONG -1

using std::vector;
using std::string;

class Room;
class Game;
class User
{
public:
	User(string username, SOCKET sock); // set, room and game set to nullptr
	void send(string message);//send msg to user / use Heler::sendData
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm); //
	void clearRoom(); //set to nullptr room
	bool createRoom(int roomId, string roomName, int maxUsers, int questionNo, int questionTim); // return if succes, check if user connected to room if yes send msg if crash protocol - code 114
	bool joinRoom(Room* newRoom);
	void leaveRoom();// if user connected to room and set to nullptr if yes
	int closeRoom();//return num room close and -1 if crash / if user not in room -1 / set to nullptr room
	bool leaveGame();//return TRUE if game online \false if end // set to nullptr

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

};
