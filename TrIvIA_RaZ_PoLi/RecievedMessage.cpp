#include "RecievedMessage.h"
#include <string>
#include <Windows.h>
#include <vector>
#include "User.h"

using std::string;
using std::vector;

RecievedMessage::RecievedMessage(SOCKET socket, int messageCode)
{
	this->_socket = socket; 
	this->_messageCode = messageCode;
	this->_user = nullptr;
}

RecievedMessage::RecievedMessage(SOCKET socket, int messageCode, vector<string> values)
{
	this->_socket = socket;
	this->_messageCode = messageCode;
	this->_values = values;
	this->_user = nullptr;
}

SOCKET RecievedMessage::getSocket()
{
	return this->_socket;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}

void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}

User* RecievedMessage::getUser()
{
	return this->_user;
}