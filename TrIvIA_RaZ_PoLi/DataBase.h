#pragma once
#include "Question.h"
#include "Validator.h"
#include <vector>
#include <string>
#include <iostream>
#include "Helper.h"
using std::string;
using std::vector;
class user;
class DataBase
{
public:
	DataBase();
	~DataBase(); //close connection to DB
	DataBase(const DataBase& other);
	bool isUserExist(string username); //check if exist in DB
	bool addNewUser(string username, string password, string email); //save data to DB
	bool isUserAndPassMatch(string username, string password);//bool -> check if username correct with password
	vector<Question*> initQuestions(int o); //random select question, o - > num question to 
	vector<string> getBestScores(); //like we want
	vector<string> getPersonalStatus(string status); // like we want
	int insertNewGame();//return num room, create new game table, status game = 0H, start = NOW 
	bool updateGameStatus(int gameId);//return if succes or no, update status to 1 and end time to NOW
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);//return if succes or no , add

private:
	static int callbackCount(void*, int, char**, char**);
	static int callbackQuestions(void * output, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);
};
